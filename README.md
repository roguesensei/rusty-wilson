# rusty-wilson

![Dynamic TOML Badge](https://img.shields.io/badge/dynamic/toml?url=https%3A%2F%2Fgitlab.com%2Froguesensei%2Frusty-wilson%2F-%2Fraw%2Fmaster%2FCargo.toml%3Fref_type%3Dheads&query=%24.package.version&style=flat-square&label=version)
![GitLab License](https://img.shields.io/gitlab/license/roguesensei%2Frusty-wilson?style=flat-square&color=%23a32d2a)

Tired of proprietary discord bots? Looking for something that's free and open source? This is the bot for you! For the original python version, see [here](https://gitlab.com/roguesensei/wilson)
## About Wilson
Wilson originally started as a simple college project which grew in size and went under a few rewrites, each as a closed-source codebase. With the recent revival of discord.py and the 2.0 update, I decided that for the next rewrite coinciding with the discord.py 2.0 update I would make Wilson entirely free and open source, especially since I myself was sick of proprietary closed-source bots (which for all I knew, just farmed data or hid features behind a paywall).

After many rewrites in Python, Wilson has been remade in Rust, with a heavier emphasis on slash commands while still staying true to the original Python version. I encourage you to clone, modify and contribute to the project yourself, or just take the source code and make your own bot from it. I don't mind, so long as you adhere to the GNU GPLv3 as specified in the `LICENSE`. Happy hacking :)
## Prerequisites and Dependencies
Wilson is designed to be as self-hostable as possible, so it's recommended to install [Docker](https://www.docker.com/get-started/) to run Wilson as a Docker container. Alternatively, you can install [Rust](https://www.rust-lang.org/learn/get-started) to compile a binary or executable.

You will also need to set an environment variable on your system named `DISCORD_BOT_TOKEN` to store your bot token with if you're not using Docker. With Docker, you will need to pass it in as an argument when running the container (See [Building Using Docker](#building-using-docker-recommended)).

For voice commands, Wilson uses [songbird](https://github.com/serenity-rs/songbird), thus you will need to install [Opus](https://github.com/lakelezz/audiopus) and yt-dlp/youtube-dl (See [songbird README](https://github.com/serenity-rs/songbird?tab=readme-ov-file#dependencies) for more details).

To use the `/voice tts` command, you will need to download the relevant models from [here](https://huggingface.co/rhasspy/piper-voices/tree/main). For detailed instructions to set up Piper for TTS, see [Configuring Piper](https://gitlab.com/roguesensei/rusty-wilson/-/wikis/Configuring-Piper) on the wiki.

## Running
Using Docker, use the sample compose and env files available on [this snippet](https://gitlab.com/roguesensei/rusty-wilson/-/snippets/4789695) - make sure they are stored in the same directory.

Run the container with
```sh
docker compose up -d
```
## Building
For compilation instructions, see [BUILDING.md](https://gitlab.com/roguesensei/rusty-wilson/-/blob/master/BUILDING.md)

## Environment Variables
The following environment variables can be set to configure Wilson:
| Var name                           | Required    | Description                                                                                                            |
|------------------------------------|-------------|------------------------------------------------------------------------------------------------------------------------|
| `DISCORD_BOT_TOKEN`                | Yes         | Discord API bot token (Required)                                                                                       |
| `SQLITE_DIR`                       | Docker only | Directory to contain SQLite database                                                                                   |
| `PIPER_DIR`                        | Docker only | Directory to contain Piper models                                                                                      |
| `DATABASE_URL`                     | Yes         | Sqlite database url                                                                                                    |
| `DEBUG_GUILD_ID`                   | No          | Developer variable - a guild id to sync slash commands to when the bot runs.                                           |
| `BOT_CONF_DEFAULT_ONLINE_STATUS`   | No          | Default Online Status of bot. Valid values are: `dnd`, `idle`, `invisible`, `offline`, `online`                        |
| `BOT_CONF_DEFAULT_ACTIVITY_TYPE`   | No          | Default Activity Type of bot. Valid values are: `playing`, `streaming`, `listening`, `watching`, `custom`, `competing` |
| `BOT_CONF_DEFAULT_ACTIVITY_NAME`   | No          | Default Activity Name of bot.                                                                                          |
| `BOT_CONF_PIPER_CONF_PATH`         | No          | Path to piper config to use for TTS. If using docker, this can be the file name relative to `PIPER_DIR`                |
