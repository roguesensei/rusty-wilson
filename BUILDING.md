# Building
## Building Using Docker (Recommended)
If you're using docker, you can build the image with
```sh
docker build -t wilson:tagname ./path/to/Dockerfile
```
### Building the container
For quick start, you can run with:
```sh
docker run -d -e "DISCORD_BOT_TOKEN=<YOUR TOKEN>" wilson:tagname
```
Alternatively, you can use docker compose. To use, create a `docker-compose.yml` and `.env` file like the samples on [this snippet](https://gitlab.com/roguesensei/rusty-wilson/-/snippets/4789695) - replace the `image` entry with the name of the image you built, for example:
```yml
services:
    wilson:
        image: 'wilson:latest'
    # ...
```
Then, to build the container run:
```sh
docker compose up -d
```
## Building Using Cargo
```sh
cargo build --release
```
OR
```sh
cargo install --path .
```
## Detailed Guides
For more detailed guides on specific topics such as configuring TTS, see [Guides](https://gitlab.com/roguesensei/rusty-wilson/-/wikis/Guides) on the wiki.
