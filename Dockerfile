FROM debian:bookworm-slim AS runtime
RUN apt update && apt install -y libc-bin libc6 libopus-dev espeak-ng espeak-ng-data python3-pip
RUN pip3 install yt-dlp --break-system-packages # TODO kinda hacky, would rather not force - but works for now
RUN rm -rf /var/lib/apt/lists/*

FROM rust:1.85 AS builder
WORKDIR /usr/src/wilson
COPY . .

RUN apt update && apt install -y build-essential autoconf automake libtool m4 cmake clang
RUN rm -rf /var/lib/apt/lists/*
RUN cargo install --path .

FROM runtime
COPY --from=builder /usr/local/cargo/bin/wilson /usr/local/bin/wilson
COPY --from=builder /usr/src/wilson/migrations /usr/local/bin/migrations
ENTRYPOINT ["wilson"]
