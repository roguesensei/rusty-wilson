use crate::Ctx;

use poise::serenity_prelude::builder::{CreateEmbed, /*CreateEmbedAuthor,*/ CreateEmbedFooter,};

pub fn create_default_embed(title: String, ctx: &Ctx<'_>) -> CreateEmbed {
    let bot_user = ctx.cache().current_user();
    let footer = CreateEmbedFooter::new(format!("Brought to you by {}", bot_user.name))
        .icon_url(bot_user.face());

    CreateEmbed::new()
        .title(title)
        // .author(author)
        .footer(footer)
}
