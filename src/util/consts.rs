use poise::serenity_prelude as serenity;

pub const BOT_INTENTS: serenity::GatewayIntents =
    serenity::GatewayIntents::non_privileged().union(serenity::GatewayIntents::GUILD_MEMBERS);

pub const BOT_TOK_VAR_NAME: &str = "DISCORD_BOT_TOKEN";
pub const DEBUG_GUILD_VAR_NAME: &str = "DEBUG_GUILD_ID";
pub const BOT_CONF_DEFAULT_ONLINE_STATUS_VAR_NAME: &str = "BOT_CONF_DEFAULT_ONLINE_STATUS";
pub const BOT_CONF_DEFAULT_ACTIVITY_TYPE_VAR_NAME: &str = "BOT_CONF_DEFAULT_ACTIVITY_TYPE";
pub const BOT_CONF_DEFAULT_ACTIVITY_NAME_VAR_NAME: &str = "BOT_CONF_DEFAULT_ACTIVITY_NAME";
pub const BOT_CONF_PIPER_CONF_PATH_VAR_NAME: &str = "BOT_CONF_PIPER_CONF_PATH";
pub const BOT_CONF_PIPER_SPEAKER_ID_VAR_NAME: &str = "BOT_CONF_PIPER_SPEAKER_ID";
pub const BOT_VERSION: &str = env!("CARGO_PKG_VERSION");

pub const AUTOROLE_DEFAULT_EMOJIS: [&str; 5] = ["😀", "🥰", "😇", "😄", "🙂"];
