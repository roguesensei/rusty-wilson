use poise::serenity_prelude as serenity;
use serenity::{ActivityData, ActivityType, OnlineStatus};
use url::Url;

use crate::Data;

#[derive(Debug, poise::ChoiceParameter)]
pub enum BotActivityType {
    #[name = "Playing"]
    Playing,
    #[name = "Streaming"]
    Streaming,
    #[name = "Listening"]
    Listening,
    #[name = "Watching"]
    Watching,
    #[name = "Custom"]
    Custom,
    #[name = "Competing"]
    Competing,
}

#[derive(Debug, poise::ChoiceParameter)]
pub enum BotStatus {
    #[name = "Do Not Disturb"]
    DoNotDisturb,
    #[name = "Idle"]
    Idle,
    #[name = "Invisible"]
    Invisible,
    #[name = "Offline"]
    Offline,
    #[name = "Online"]
    Online,
}

pub fn bot_activity_to_discord_native(
    bot_activity_type: &BotActivityType,
    name: &String,
    url: Option<Url>,
) -> ActivityData {
    let kind = match bot_activity_type {
        BotActivityType::Playing => ActivityType::Playing,
        BotActivityType::Streaming => ActivityType::Streaming,
        BotActivityType::Listening => ActivityType::Listening,
        BotActivityType::Watching => ActivityType::Watching,
        BotActivityType::Custom => ActivityType::Custom,
        BotActivityType::Competing => ActivityType::Competing,
    };

    let state = match bot_activity_type {
        BotActivityType::Custom => Some(name.to_owned()),
        _ => None,
    };

    ActivityData {
        name: name.to_owned(),
        kind,
        state,
        url,
    }
}

pub fn bot_status_to_discord_native(bot_status: &BotStatus) -> OnlineStatus {
    match bot_status {
        BotStatus::DoNotDisturb => OnlineStatus::DoNotDisturb,
        BotStatus::Idle => OnlineStatus::Idle,
        BotStatus::Invisible => OnlineStatus::Invisible,
        BotStatus::Offline => OnlineStatus::Offline,
        BotStatus::Online => OnlineStatus::Online,
    }
}

pub fn get_config_activity_defaults(data: &Data) -> (Option<ActivityData>, OnlineStatus) {
    let mut activity: Option<ActivityData> = None;
    if let Some(default_activity_name) = &data.config.default_activity_name {
        if default_activity_name.trim() != "" {
            activity = Some(bot_activity_to_discord_native(
                &data.config.default_activity_type,
                default_activity_name,
                None,
            ));
        }
    }
    let status = bot_status_to_discord_native(&data.config.default_online_status);

    (activity, status)
}
