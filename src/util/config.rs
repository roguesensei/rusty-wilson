use std::env;

use super::{
    activity::{BotActivityType, BotStatus},
    consts::{
        BOT_CONF_DEFAULT_ACTIVITY_NAME_VAR_NAME, BOT_CONF_DEFAULT_ACTIVITY_TYPE_VAR_NAME,
        BOT_CONF_DEFAULT_ONLINE_STATUS_VAR_NAME,
    },
};

pub struct BotConfig {
    pub default_activity_name: Option<String>,
    pub default_activity_type: BotActivityType,
    pub default_online_status: BotStatus,
}

pub fn load_config() -> BotConfig {
    let mut default_online_status = BotStatus::Online;
    let mut default_activity_type: BotActivityType = BotActivityType::Playing;
    if let Ok(var) = env::var(BOT_CONF_DEFAULT_ONLINE_STATUS_VAR_NAME) {
        default_online_status = match var.trim() {
            "dnd" => BotStatus::DoNotDisturb,
            "idle" => BotStatus::Idle,
            "invisible" => BotStatus::Invisible,
            "offline" => BotStatus::Offline,
            "online" => BotStatus::Online,
            _ => panic!("Invalid value for ${BOT_CONF_DEFAULT_ONLINE_STATUS_VAR_NAME}\nValid values are: [\"dnd\", \"idle\", \"invisible\", \"offline\" \"online\"]"),
        };
    }

    if let Ok(var) = env::var(BOT_CONF_DEFAULT_ACTIVITY_TYPE_VAR_NAME) {
        default_activity_type = match var.trim() {
            "playing" => BotActivityType::Playing,
            "streaming" => BotActivityType::Streaming,
            "listening" => BotActivityType::Listening,
            "watching" => BotActivityType::Watching,
            "custom" => BotActivityType::Custom,
            "competing" => BotActivityType::Competing,
            _ => panic!("Invalid value for ${BOT_CONF_DEFAULT_ACTIVITY_TYPE_VAR_NAME}\nValid values are : [\"playing\", \"streaming\", \"listening\", \"watching\", \"custom\", \"competing\"]")
        }
    }

    let default_activity_name = if let Ok(var) = env::var(BOT_CONF_DEFAULT_ACTIVITY_NAME_VAR_NAME) {
        Some(var)
    } else {
        None
    };

    if let Some(name) = &default_activity_name {
        if name.is_empty() || name.len() > 32 {
            panic!("Invalid value for ${BOT_CONF_DEFAULT_ACTIVITY_NAME_VAR_NAME} - Value must be between 1 and 32 characters");
        }
    }

    BotConfig {
        default_activity_name,
        default_activity_type,
        default_online_status,
    }
}
