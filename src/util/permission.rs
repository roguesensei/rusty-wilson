use poise::serenity_prelude::{Guild, Member, Role};

pub fn compare_member_to_role_permissions(guild: &Guild, member: &Member, role: &Role) -> i32 {
    let member_top_role = get_member_highest_role(guild, member);
    compare_role_to_role(member_top_role, role)
}

pub fn compare_member_to_member_permissions(
    guild: &Guild,
    member0: &Member,
    member1: &Member,
) -> i32 {
    let member0_top_role = get_member_highest_role(guild, member0);
    let member1_top_role = get_member_highest_role(guild, member1);

    compare_role_to_role(member0_top_role, member1_top_role)
}

fn get_member_highest_role<'a>(guild: &'a Guild, member: &'a Member) -> &'a Role {
    guild
        .member_highest_role(member)
        .expect("Could not determine role for member")
}

fn compare_role_to_role(role0: &Role, role1: &Role) -> i32 {
    if role0.position > role1.position {
        1
    } else if role0.position < role1.position {
        -1
    } else {
        0
    }
}
