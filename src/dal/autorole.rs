use super::util::get_sqlite_pool;

#[derive(Debug, sqlx::FromRow)]
pub struct AutoRoleId {
    pub guild_settings_autorole_id: i32,
}

#[derive(Debug, sqlx::FromRow)]
pub struct AutoRole {
    pub role_id: u64,
}

pub async fn get_autorole_by_unicode_emoji(
    message_id: u64,
    unicode_emoji_str: &String,
) -> Result<Option<AutoRole>, sqlx::Error> {
    let query = r#"
SELECT
    d.role_id
FROM guild_settings_autorole_detail d
JOIN guild_settings_autorole s INDEXED BY idx_guild_settings_autorole_message_id
ON
    d.guild_settings_autorole_id = s.guild_settings_autorole_id
WHERE
    s.message_id = ?1 AND
    d.unicode_emoji_str = ?2 AND
    d.reaction_type = 1
		"#;

    let pool = get_sqlite_pool().await;
    sqlx::query_as::<_, AutoRole>(query)
        .bind(message_id as i64)
        .bind(unicode_emoji_str)
        .fetch_optional(&pool)
        .await
}

pub async fn create_autorole(
    guild_id: u64,
    message_id: u64,
    name: String,
) -> Result<Option<AutoRoleId>, sqlx::Error> {
    let query = r#"
INSERT INTO guild_settings_autorole
(
	guild_settings_id,
	message_id,
	name
)
VALUES
(
	(SELECT guild_settings_id FROM guild_settings INDEXED BY idx_guild_settings_guild_id WHERE guild_id = ?1),
	?2,
	?3
);
SELECT LAST_INSERT_ROWID() as 'guild_settings_autorole_id';
		"#;

    let pool = get_sqlite_pool().await;
    sqlx::query_as::<_, AutoRoleId>(query)
        .bind(guild_id as i64)
        .bind(message_id as i64)
        .bind(name)
        .fetch_optional(&pool)
        .await
}

pub async fn create_autorole_detail(
    guild_settings_autorole_id: &i32,
    unicode_emoji_str: &String,
    role_id: &u64,
) -> Result<(), sqlx::Error> {
    let query = r#"
INSERT INTO guild_settings_autorole_detail
(
	guild_settings_autorole_id,
	reaction_type,
	unicode_emoji_str,
	role_id
)
VALUES
(
	?1,
	1,
	?2,
	?3
);
"#;

    let pool = get_sqlite_pool().await;
    let res = sqlx::query(query)
        .bind(guild_settings_autorole_id)
        .bind(unicode_emoji_str)
        .bind(*role_id as i64)
        .execute(&pool)
        .await;

    if let Err(why) = res {
        println!("{why}");
        return Err(why);
    }
    Ok(())
}
