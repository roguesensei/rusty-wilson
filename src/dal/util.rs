use sqlx::sqlite::SqlitePool;
use std::env;

pub async fn get_sqlite_pool() -> SqlitePool {
    let db_url = env::var("DATABASE_URL").expect("$DATABASE_URL is not set");
    SqlitePool::connect(&db_url)
        .await
        .expect("Could not connect to database")
}
