use std::env;

use sqlx::{migrate::MigrateDatabase, SqlitePool};

pub async fn run() {
    let db_url = env::var("DATABASE_URL").expect("$DATABASE_URL is not set");

    if !sqlx::Sqlite::database_exists(&db_url)
        .await
        .expect("Could not verify database existence")
    {
        sqlx::Sqlite::create_database(&db_url)
            .await
            .expect("Could not create the database");
    }

    let db = SqlitePool::connect(&db_url)
        .await
        .expect("Could not connect to database");

    let migrations = std::env::current_exe()
        .expect("Could not retrieve the runtime exe")
        .parent()
        .expect("Could not retrieve the runtime directory")
        .join("migrations/");

    sqlx::migrate::Migrator::new(migrations)
        .await
        .expect("Could not initialise the migrator")
        .run(&db)
        .await
        .expect("Could not migrate the database");
}
