use super::util::get_sqlite_pool;

#[derive(Debug, sqlx::FromRow)]
pub struct GuildSettings {
    pub guild_id: i64,
    pub enable_welcome_actions: bool,
    pub welcome_message: Option<String>,
    pub welcome_channel_id: Option<i64>,
    pub welcome_role_id: Option<i64>,
}

pub async fn get_guild_settings(guild_id: i64) -> Result<Option<GuildSettings>, sqlx::Error> {
    let pool = get_sqlite_pool().await;
    let query = "
SELECT
    guild_id,
    enable_welcome_actions,
    welcome_message,
    welcome_channel_id,
    welcome_role_id
FROM guild_settings INDEXED BY idx_guild_settings_guild_id
WHERE guild_id = ?1
    ";

    sqlx::query_as::<_, GuildSettings>(query)
        .bind(guild_id)
        .fetch_optional(&pool)
        .await
}

pub async fn create_guild_settings(guild_id: i64) -> Result<(), sqlx::Error> {
    let query = "
INSERT INTO guild_settings
(
    guild_id,
    enable_welcome_actions,
    welcome_message,
    welcome_channel_id,
    welcome_role_id
)
VALUES
(
    ?1,
    FALSE,
    NULL,
    NULL,
    NULL
)
    ";

    run_generic_crud_query(guild_id, query).await
}

pub async fn update_guild_settings(settings: &GuildSettings) -> Result<(), sqlx::Error> {
    let pool = get_sqlite_pool().await;
    let query = "
UPDATE guild_settings
SET
    enable_welcome_actions = ?1,
    welcome_role_id = ?2,
    welcome_channel_id = ?3,
    welcome_message = ?4
WHERE guild_id = ?5
    ";

    let res = sqlx::query(query)
        .bind(settings.enable_welcome_actions)
        .bind(settings.welcome_role_id)
        .bind(settings.welcome_channel_id)
        .bind(&settings.welcome_message)
        .bind(settings.guild_id)
        .execute(&pool)
        .await;

    if let Err(why) = res {
        println!("{why}");
        return Err(why);
    }
    Ok(())
}

pub async fn delete_guild_settings(guild_id: i64) -> Result<(), sqlx::Error> {
    let query = "
DELETE FROM guild_settings
WHERE guild_id = ?1
    ";
    run_generic_crud_query(guild_id, query).await
}

async fn run_generic_crud_query(guild_id: i64, query: &str) -> Result<(), sqlx::Error> {
    let pool = get_sqlite_pool().await;
    if let Err(why) = sqlx::query(query).bind(guild_id).execute(&pool).await {
        println!("{why}");
        return Err(why);
    }
    Ok(())
}
