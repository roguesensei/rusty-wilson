use poise::serenity_prelude as serenity;
use url::Url;

use crate::{
    util::activity::{
        bot_activity_to_discord_native, bot_status_to_discord_native, get_config_activity_defaults,
        BotActivityType, BotStatus,
    },
    Ctx, Err,
};

#[poise::command(
    slash_command,
    subcommands("activity", "status", "reset_activity"),
    subcommand_required,
    owners_only
)]
pub async fn bot(_: Ctx<'_>) -> Result<(), Err> {
    Ok(())
}

/// Set bot activity
#[poise::command(slash_command)]
async fn activity(
    ctx: Ctx<'_>,
    #[description = "Activity type"] activity_type: BotActivityType,
    #[description = "Activity Name"]
    #[max_length = 32]
    name: String,
    #[description = "Bot online status"] status: Option<BotStatus>,
    #[description = "Stream URL (For \"Streaming\" activity_type)"] stream_url: Option<String>,
) -> Result<(), Err> {
    let mut url: Option<Url> = None;
    if let Some(stream) = stream_url {
        match Url::parse(stream.as_str()) {
            Ok(u) => url = Some(u),
            Err(why) => {
                eprintln!("{why:?}");
            }
        }
    }

    let activity_data = bot_activity_to_discord_native(&activity_type, &name, url);
    let online_status = if let Some(stat) = status {
        bot_status_to_discord_native(&stat)
    } else {
        serenity::OnlineStatus::Online
    };

    ctx.serenity_context()
        .set_presence(Some(activity_data), online_status);

    ctx.say("Updated activity").await?;
    Ok(())
}

/// Set bot online status
#[poise::command(slash_command)]
async fn status(
    ctx: Ctx<'_>,
    #[description = "Bot online status"] status: BotStatus,
) -> Result<(), Err> {
    ctx.serenity_context()
        .set_presence(None, bot_status_to_discord_native(&status));

    ctx.say("Updated status").await?;
    Ok(())
}

/// Reset bot activity to config default
#[poise::command(slash_command)]
async fn reset_activity(ctx: Ctx<'_>) -> Result<(), Err> {
    let (activity, status) = get_config_activity_defaults(ctx.data());
    ctx.serenity_context().set_presence(activity, status);

    ctx.say("Reset status").await?;
    Ok(())
}
