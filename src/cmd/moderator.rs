use crate::{
    dal,
    util::{
        consts::AUTOROLE_DEFAULT_EMOJIS,
        permission::{compare_member_to_member_permissions, compare_member_to_role_permissions},
    },
    Ctx, Err,
};
use poise::serenity_prelude as serenity;
use std::{
    collections::{HashMap, HashSet},
    time::Duration,
};

#[derive(Debug, poise::Modal)]
struct AutoroleNameWrapperModal {
    pub name: String,
}

#[poise::command(
    slash_command,
    subcommands("give", "take", "autorole"),
    required_permissions = "MANAGE_ROLES",
    subcommand_required
)]
pub async fn role(_: Ctx<'_>) -> Result<(), Err> {
    Ok(())
}

/// Give a role to a user
#[poise::command(slash_command)]
pub async fn give(
    ctx: Ctx<'_>,
    #[description = "User to manage roles of"] user: serenity::Member,
    #[description = "Role to apply to user"] role: serenity::Role,
) -> Result<(), Err> {
    if can_edit_user_role(&ctx, &user, &role).await.unwrap() {
        if !user
            .user
            .has_role(&ctx.http(), role.guild_id, role.id)
            .await
            .unwrap()
        {
            if let Err(why) = user.add_role(&ctx.http(), role.id).await {
                ctx.say("Could not apply role to user").await?;

                eprintln!("{why:?}");
            } else {
                ctx.say(format!(
                    "Applied role **{}** to **{}**",
                    role.name,
                    user.display_name(),
                ))
                .await?;
            }
        } else {
            ctx.say(format!("**{}** already has this role", user.display_name()))
                .await?;
        }
    }

    Ok(())
}

/// Revoke a role from a user
#[poise::command(slash_command)]
pub async fn take(
    ctx: Ctx<'_>,
    #[description = "User to manage roles of"] user: serenity::Member,
    #[description = "Role to remove from user"] role: serenity::Role,
) -> Result<(), Err> {
    if can_edit_user_role(&ctx, &user, &role).await.unwrap() {
        if user
            .user
            .has_role(&ctx.http(), role.guild_id, role.id)
            .await
            .unwrap()
        {
            if let Err(why) = user.remove_role(&ctx.http(), role.id).await {
                ctx.say("Could not remove role from user").await?;

                eprintln!("{why:?}");
            } else {
                ctx.say(format!(
                    "Revoked role **{}** from **{}**",
                    role.name,
                    user.display_name(),
                ))
                .await?;
            }
        } else {
            ctx.say(format!(
                "**{}** does not have this role",
                user.display_name()
            ))
            .await?;
        }
    }

    Ok(())
}

/// Set up autorole
#[poise::command(slash_command)]
async fn autorole(
    ctx: Ctx<'_>,
    #[description = "Number of roles to include in autorole"]
    #[min = 1]
    #[max = 5]
    num_roles: usize,
) -> Result<(), Err> {
    let uid = ctx.id();
    let mut idx = 0;
    let mut components = vec![];
    let mut map: HashMap<String, Option<u64>> = HashMap::new();

    loop {
        if idx == num_roles {
            break;
        }
        idx += 1;

        let key = format!("autorole{uid}_{idx}");
        map.insert(key.to_owned(), None);
        components.push(serenity::CreateActionRow::SelectMenu(
            serenity::CreateSelectMenu::new(
                key,
                serenity::CreateSelectMenuKind::Role {
                    default_roles: None,
                },
            ),
        ));
    }

    let btn_id = format!("btn{uid}");
    components.push(serenity::CreateActionRow::Buttons(vec![
        serenity::CreateButton::new(&btn_id)
            .style(serenity::ButtonStyle::Primary)
            .label("Submit"),
    ]));

    let builder = poise::CreateReply::default().components(components);
    let reply = ctx.send(builder).await.unwrap();
    while let Some(collector) = serenity::ComponentInteractionCollector::new(ctx)
        .author_id(ctx.author().id)
        .channel_id(ctx.channel_id())
        .timeout(Duration::from_secs(120))
        .await
    {
        let key = &collector.data.custom_id;
        if *key == btn_id {
            let name_wrapper = poise::execute_modal_on_component_interaction::<
                AutoroleNameWrapperModal,
            >(ctx, collector, None, None)
            .await?;

            let mut some: Vec<_> = map
                .values()
                .flatten()
                .collect();
            let set: HashSet<_> = some.drain(..).collect();

            let mut builder = serenity::MessageBuilder::new();
            let mut i = 0;
            for role_id in &set {
                let emoji = AUTOROLE_DEFAULT_EMOJIS[i];
                builder.push_line(format!("- {emoji} <@&{role_id}>"));
                i += 1;
            }

            let reply = poise::CreateReply::default().content(builder.build());
            let message = ctx.send(reply).await?.into_message().await?;
            let autorole_base = dal::autorole::create_autorole(
                ctx.guild_id().unwrap().get(),
                message.id.get(),
                name_wrapper.unwrap().name,
            )
            .await?;
            let autorole_id = autorole_base.unwrap().guild_settings_autorole_id;

            i = 0;
            for role_id in &set {
                let emoji = AUTOROLE_DEFAULT_EMOJIS[i].to_string();
                dal::autorole::create_autorole_detail(&autorole_id, &emoji, role_id).await?;
                message
                    .react(ctx, serenity::ReactionType::Unicode(emoji))
                    .await?;
                i += 1;
            }

            break;
        } else {
            if let serenity::ComponentInteractionDataKind::RoleSelect { values } =
                &collector.data.kind
            {
                let role_id = values[0].get();
                map.entry(key.to_owned()).and_modify(|r| *r = Some(role_id));
            }

            collector
                .create_response(ctx, serenity::CreateInteractionResponse::Acknowledge)
                .await?;
        }
    }

    reply.delete(ctx).await?;

    Ok(())
}

/// Ban a user from the server
#[poise::command(slash_command, required_permissions = "BAN_MEMBERS")]
pub async fn ban(
    ctx: Ctx<'_>,
    #[description = "User to ban from server"] user: serenity::Member,
) -> Result<(), Err> {
    if let Err(why) = user.ban(ctx.http(), 0).await {
        ctx.say(format!("Error banning member: {why}")).await?;
    } else {
        ctx.say("Banned user from the server").await?;
    }
    Ok(())
}

/// Kick a user from the server
#[poise::command(slash_command, required_permissions = "KICK_MEMBERS")]
pub async fn kick(
    ctx: Ctx<'_>,
    #[description = "User to kick from server"] user: serenity::Member,
) -> Result<(), Err> {
    if let Err(why) = user.kick(ctx.http()).await {
        ctx.say(format!("Error kicking member: {why}")).await?;
    } else {
        ctx.say("Kicked user from the server").await?;
    }
    Ok(())
}

/// Prune messages in a channel
#[poise::command(slash_command, required_permissions = "MANAGE_MESSAGES")]
pub async fn prune(
    ctx: Ctx<'_>,
    #[description = "Default Bulk - Limited to 100 messages <14 days old, use \"Slow\" to get around limitations"]
    prune_mode: Option<PruneMode>,
    #[description = "Number of messages to prune (2-100)"]
    #[min = 2]
    #[max = 100]
    count: u8,
) -> Result<(), Err> {
    match ctx
        .http()
        .get_messages(ctx.channel_id(), None, Some(count))
        .await
    {
        Ok(messages) => {
            let mode = prune_mode.unwrap_or(PruneMode::Bulk);
            let success = match mode {
                PruneMode::Bulk => {
                    let mut result = true;
                    let mut message_ids: Vec<serenity::MessageId> = vec![];
                    messages.iter().for_each(|f| message_ids.push(f.id));

                    if let Some(channel) = ctx.guild_channel().await {
                        if let Err(why) = channel.delete_messages(ctx.http(), message_ids).await {
                            ctx.say(format!("{why}")).await?;
                            result = false;
                        }
                    } else {
                        ctx.say("Not a valid channel").await?;
                        result = false;
                    }

                    result
                }
                PruneMode::Slow => {
                    let mut result = true;
                    for message in messages {
                        if let Err(why) = message.delete(ctx.http()).await {
                            eprintln!("{why:?}");
                            result = false;
                        }
                    }

                    if !result {
                        ctx.say("One or more errors occurred while deleting the messages, some may not have been deleted.").await?;
                    }
                    result
                }
            };
            if success {
                ctx.say(format!("Pruned {count} messages")).await?;
            }
        }
        Err(why) => {
            eprintln!("{why:?}");
            ctx.say("Could not retrieve any messages").await?;
        }
    }
    Ok(())
}

/// Change a user's nickname
#[poise::command(slash_command, required_permissions = "MANAGE_NICKNAMES")]
pub async fn nick(
    ctx: Ctx<'_>,
    #[description = "User to update nickname of"] mut user: serenity::Member,
    #[description = "New nickname"]
    #[max_length = 32]
    new_nick: String,
) -> Result<(), Err> {
    let edit = serenity::EditMember::new().nickname(&new_nick);
    if let Err(why) = user.edit(ctx.http(), edit).await {
        ctx.say(format!("Could not edit the user's nickname: {why}"))
            .await?;
    } else {
        ctx.say(format!("Updated nickname to **{new_nick}**"))
            .await?;
    }

    Ok(())
}

async fn can_edit_user_role(
    ctx: &Ctx<'_>,
    user: &serenity::Member,
    role: &serenity::Role,
) -> Result<bool, Err> {
    let mut result = true;
    let author_member = ctx.author_member().await.unwrap();
    if compare_member_to_role_permissions(
        &ctx.guild().expect("Could not retrieve the guild"),
        &author_member,
        role,
    ) <= 0
    {
        ctx.say("Cannot set a user's role to your own or higher")
            .await?;

        result = false;
    } else if compare_member_to_member_permissions(
        &ctx.guild().expect("Could not retrieve the guild"),
        &author_member,
        user,
    ) < 0
    {
        ctx.say("Cannot assign a role to a user with higher or equal privilige")
            .await?;

        result = false;
    }

    Ok(result)
}

#[derive(Debug, poise::ChoiceParameter)]
enum PruneMode {
    #[name = "Bulk"]
    Bulk,
    #[name = "Slow"]
    Slow,
}
