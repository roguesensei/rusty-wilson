use std::{env, path::Path};

use piper_rs::synth::PiperSpeechSynthesizer;
use poise::serenity_prelude as serenity;
use songbird::{
    events::{Event, EventContext, EventHandler as VoiceEventHandler, TrackEvent},
    input::{File, YoutubeDl},
    tracks::Track,
};

use crate::{
    util::consts::{BOT_CONF_PIPER_CONF_PATH_VAR_NAME, BOT_CONF_PIPER_SPEAKER_ID_VAR_NAME},
    Ctx, Err,
};

#[poise::command(
    slash_command,
    subcommands("join", "leave", "mute", "unmute", "tts"),
    subcommand_required,
    guild_only
)]
pub async fn voice(_: Ctx<'_>) -> Result<(), Err> {
    Ok(())
}

#[poise::command(
    slash_command,
    subcommands("queue", "skip", "pause", "resume", "stop"),
    subcommand_required,
    guild_only
)]
pub async fn music(_: Ctx<'_>) -> Result<(), Err> {
    Ok(())
}

/// Join current voice channel
#[poise::command(slash_command)]
async fn join(ctx: Ctx<'_>) -> Result<(), Err> {
    ctx.defer().await?;
    let (guild_id, channel_id) = {
        let guild = ctx.guild().unwrap();
        let channel_id = guild
            .voice_states
            .get(&ctx.author().id)
            .and_then(|voice_state| voice_state.channel_id);

        (guild.id, channel_id)
    };

    let connect_to = match channel_id {
        Some(channel) => channel,
        None => {
            ctx.say("Not in a voice channel").await?;

            return Ok(());
        }
    };

    let manager = &ctx.data().songbird;
    if let Ok(handler_lock) = manager.join(guild_id, connect_to).await {
        // Attach an event handler to see notifications of all track errors.
        let mut handler = handler_lock.lock().await;
        if !handler.is_deaf() {
            handler.deafen(true).await.expect("Failed to deafen");
        }
        handler.add_global_event(TrackEvent::Error.into(), TrackErrorNotifier);
        ctx.say("Joined voice channel").await?;
    }

    Ok(())
}

/// Mute voice playback
#[poise::command(slash_command)]
async fn mute(ctx: Ctx<'_>) -> Result<(), Err> {
    if let Some(handler_lock) = load_voice_handler(&ctx).await {
        let mut handler = handler_lock.lock().await;
        if !handler.is_mute() {
            handler.mute(true).await?;
            ctx.say("Muted").await?;
        } else {
            ctx.say("Already muted").await?;
        }
    }
    Ok(())
}

/// Unmute voice playback
#[poise::command(slash_command)]
async fn unmute(ctx: Ctx<'_>) -> Result<(), Err> {
    if let Some(handler_lock) = load_voice_handler(&ctx).await {
        let mut handler = handler_lock.lock().await;
        if handler.is_mute() {
            handler.mute(false).await?;
            ctx.say("Unmuted").await?;
        } else {
            ctx.say("Already unmuted").await?;
        }
    }
    Ok(())
}

/// Leave current voice channel
#[poise::command(slash_command)]
async fn leave(ctx: Ctx<'_>) -> Result<(), Err> {
    let guild_id = ctx.guild_id().unwrap();

    let manager = &ctx.data().songbird;
    if manager.get(guild_id).is_some() {
        if let Err(why) = manager.remove(guild_id).await {
            ctx.say(format!("Failed: {:?}", why)).await?;
        } else {
            ctx.say("Left voice channel").await?;
        }
    } else {
        ctx.say("Not in a voice channel").await?;
    }

    Ok(())
}

/// Turn text to speech using Piper
#[poise::command(slash_command)]
async fn tts(
    ctx: Ctx<'_>,
    #[description = "Message to speak"]
    #[max_length = 2000]
    text: String,
) -> Result<(), Err> {
    if let Ok(conf) = env::var(BOT_CONF_PIPER_CONF_PATH_VAR_NAME) {
        if let Some(handler_lock) = load_voice_handler(&ctx).await {
            ctx.defer().await?;

            let model = piper_rs::from_config_path(Path::new(&conf))?;

            if let Ok(sid) = env::var(BOT_CONF_PIPER_SPEAKER_ID_VAR_NAME) {
                let sid = sid.parse::<i64>();
                if let Ok(sid) = sid {
                    model.set_speaker(sid);
                } else {
                    eprintln!(
                        "Could not parse ${BOT_CONF_PIPER_SPEAKER_ID_VAR_NAME}, must be an integer"
                    );
                }
            }

            let filename = format!(".{}.wav", ctx.id());
            let path = Path::new(&filename);
            let synth = PiperSpeechSynthesizer::new(model)?;

            if let Err(why) = synth.synthesize_to_file(path, text.to_owned(), None) {
                ctx.say(format!("{why}")).await?;
            } else {
                let file = File::new(filename.to_owned());
                let track = Track::from(file);

                let mut handler = handler_lock.lock().await;
                handler.play(track);

                ctx.say(text).await?;
            }
            if let Err(why) = std::fs::remove_file(path) {
                eprintln!("{why}");
            }
        }
    } else {
        eprintln!("${BOT_CONF_PIPER_CONF_PATH_VAR_NAME} not set");
        ctx.say("Command unavailable").await?;
    }
    Ok(())
}

/// Enqueue music to play in a voice channel
#[poise::command(slash_command)]
async fn queue(
    ctx: Ctx<'_>,
    #[description = "Valid URL for YTDL"] url: Option<String>,
    #[description = "Audio file to play"] attachment: Option<serenity::Attachment>,
) -> Result<(), Err> {
    let mut ytdl_url = String::new();
    if url.is_some() {
        if !url.as_ref().unwrap().starts_with("http") {
            ctx.say("Must supply a valid URL").await?;
            return Ok(());
        }
        ytdl_url = url.unwrap();
    } else if let Some(a) = attachment {
        if let Some(ctype) = &a.content_type {
            if ctype.starts_with("audio/") {
                ytdl_url = a.url;
            } else {
                ctx.say("Unsupported file type, please upload an audio file")
                    .await?;
                return Ok(());
            }
        }
    } else {
        ctx.say("Must supply either a URL or file attachment")
            .await?;
        return Ok(());
    }
    ctx.defer().await?;

    if let Some(handler_lock) = load_voice_handler(&ctx).await {
        let mut handler = handler_lock.lock().await;
        let src = YoutubeDl::new(ctx.data().http_client.clone(), ytdl_url.to_owned());

        handler.enqueue(src.into()).await;
        ctx.say(format!(
            "Added [song]({}) to queue: position {}",
            ytdl_url,
            handler.queue().len()
        ))
        .await?;
    }
    Ok(())
}

/// Skip currently playing song
#[poise::command(slash_command)]
async fn skip(ctx: Ctx<'_>) -> Result<(), Err> {
    if let Some(handler_lock) = load_voice_handler(&ctx).await {
        let handler = handler_lock.lock().await;
        let queue = handler.queue();
        if !queue.is_empty() {
            let _ = queue.skip();
            ctx.say("Skipped Song").await?;
        } else {
            ctx.say("No song playing in queue").await?;
        }
    }
    Ok(())
}

/// Pause currently playing song
#[poise::command(slash_command)]
async fn pause(ctx: Ctx<'_>) -> Result<(), Err> {
    if let Some(handler_lock) = load_voice_handler(&ctx).await {
        let handler = handler_lock.lock().await;
        let queue = handler.queue();
        if !queue.is_empty() {
            let _ = queue.pause();
            ctx.say("Paused playback").await?;
        } else {
            ctx.say("No song playing in queue").await?;
        }
    }
    Ok(())
}

/// Resume paused song
#[poise::command(slash_command)]
async fn resume(ctx: Ctx<'_>) -> Result<(), Err> {
    if let Some(handler_lock) = load_voice_handler(&ctx).await {
        let handler = handler_lock.lock().await;
        let queue = handler.queue();
        if !queue.is_empty() {
            let _ = queue.resume();
            ctx.say("Resumed playback").await?;
        } else {
            ctx.say("No song playing in queue").await?;
        }
    }
    Ok(())
}

/// Stop audio and clear the queue
#[poise::command(slash_command)]
async fn stop(ctx: Ctx<'_>) -> Result<(), Err> {
    if let Some(handler_lock) = load_voice_handler(&ctx).await {
        let handler = handler_lock.lock().await;
        let queue = handler.queue();
        if !queue.is_empty() {
            queue.stop();
            ctx.say("Stopped music").await?;
        } else {
            ctx.say("No song playing in queue").await?;
        }
    }
    Ok(())
}

struct TrackErrorNotifier;

#[poise::async_trait]
impl VoiceEventHandler for TrackErrorNotifier {
    async fn act(&self, ctx: &EventContext<'_>) -> Option<Event> {
        if let EventContext::Track(track_list) = ctx {
            for (state, handle) in *track_list {
                println!(
                    "Track {:?} encountered an error: {:?}",
                    handle.uuid(),
                    state.playing
                );
            }
        }
        None
    }
}

async fn load_voice_handler(
    ctx: &Ctx<'_>,
) -> Option<std::sync::Arc<::serenity::prelude::Mutex<songbird::Call>>> {
    let guild_id = ctx.guild_id().unwrap();

    let manager = &ctx.data().songbird;
    let handler_lock = manager.get(guild_id);
    if handler_lock.is_none() {
        let _ = ctx.say("Not in a voice channel").await;
    }
    handler_lock
}
