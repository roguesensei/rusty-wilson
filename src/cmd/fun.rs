use crate::util::embed::create_default_embed;
use crate::{Ctx, Err};
use ::serenity::all::{CreateMessage, CreatePoll, CreatePollAnswer};
use poise::serenity_prelude as serenity;
use rand::Rng;

#[poise::command(
    slash_command,
    subcommands("avatar", "whois"),
    subcommand_required,
    guild_only
)]
pub async fn user(_: Ctx<'_>) -> Result<(), Err> {
    Ok(())
}

#[poise::command(
    slash_command,
    subcommands("icon", "info"),
    subcommand_required,
    guild_only
)]
pub async fn server(_: Ctx<'_>) -> Result<(), Err> {
    Ok(())
}

/// Get a member's avatar
#[poise::command(slash_command, guild_only)]
async fn avatar(
    ctx: Ctx<'_>,
    #[description = "Selected user"] user: serenity::Member,
) -> Result<(), Err> {
    let embed =
        create_default_embed(format!("Avatar to {}", user.user.name), &ctx).image(user.face());

    let msg = poise::CreateReply::default().embed(embed);
    ctx.send(msg).await?;

    Ok(())
}

/// Get information on a user
#[poise::command(slash_command, guild_only)]
async fn whois(
    ctx: Ctx<'_>,
    #[description = "Selected user to query"] user: serenity::Member,
) -> Result<(), Err> {
    let mut fields = vec![("Username", &user.user.name, true)];

    let disc = user.user.discriminator.as_ref().map(|d| format!("#{d}"));

    if let Some(x) = &disc {
        fields.push(("Discriminator", x, true));
    }

    if let Some(x) = &user.user.global_name {
        fields.push(("Global name", x, true));
    }

    if let Some(x) = &user.nick {
        fields.push(("Nickname", x, true));
    }

    let joined_discord = user
        .user
        .created_at()
        .format("%Y-%m-%d %H:%M:%S UTC")
        .to_string();
    fields.push(("Joined Discord", &joined_discord, true));

    let title = match &user.user.bot {
        true => String::from("Bot User Info"),
        _ => String::from("User Info"),
    };

    let embed = create_default_embed(title, &ctx)
        .fields(fields)
        .thumbnail(user.face())
        .image(user.user.banner_url().unwrap_or_default());

    let msg = poise::CreateReply::default().embed(embed);
    ctx.send(msg).await?;

    Ok(())
}

/// Get the server icon
#[poise::command(slash_command, guild_only)]
async fn icon(ctx: Ctx<'_>) -> Result<(), Err> {
    let mut msg = poise::CreateReply::default();
    if let Some(guild_id) = ctx.guild_id() {
        let guild = ctx.cache().guild(guild_id).unwrap();
        if let Some(url) = guild.icon_url() {
            let embed =
                create_default_embed(format!("{} Server Avatar", guild.name), &ctx).image(url);
            msg = msg.embed(embed);
        } else {
            msg = msg.content("Server does not have an avatar");
        }
    } else {
        msg = msg.content("Unable to retrieve server avatar");
    }

    ctx.send(msg).await?;
    //ctx.say("Unable to retrieve guild avatar").await?;
    Ok(())
}

/// Get the server information
#[poise::command(slash_command, guild_only)]
async fn info(ctx: Ctx<'_>) -> Result<(), Err> {
    let guild = ctx.guild().unwrap().clone();
    let mut fields = vec![
        ("Server Name", guild.name.to_owned(), true),
        ("Member Count", guild.member_count.to_string(), true),
        ("Role Count", (guild.roles.len() - 1).to_string(), true),
        ("Emoji Count", guild.emojis.len().to_string(), true),
        ("Sticker Count", guild.stickers.len().to_string(), true),
        ("Channel Count", guild.channels.len().to_string(), true),
    ];

    if let Ok(owner) = ctx.http().get_member(guild.id, guild.owner_id).await {
        let owner_name = owner.display_name().to_string();
        fields.push(("Server Owner", owner_name, false));
    } else {
        println!("Owner not found.");
    }

    let joined_discord = guild.joined_at.format("%Y-%m-%d %H:%M:%S UTC").to_string();
    fields.push(("Created At", joined_discord, false));

    let embed = create_default_embed("Server Info".into(), &ctx)
        .fields(fields)
        .thumbnail(guild.icon_url().unwrap_or_default())
        .image(guild.banner_url().unwrap_or_default());

    let msg = poise::CreateReply::default().embed(embed);
    ctx.send(msg).await?;

    Ok(())
}

/// View an emoji image (beta)
#[poise::command(slash_command, guild_only, owners_only)]
pub async fn emoji(
    ctx: Ctx<'_>,
    #[description = "Emoji snowflake from the server"] emoji: serenity::EmojiId,
) -> Result<(), Err> {
    if let Ok(e) = ctx.http().get_emoji(ctx.guild_id().unwrap(), emoji).await {
        let embed = create_default_embed(e.name.to_owned(), &ctx).image(e.url());

        let msg = poise::CreateReply::default().embed(embed);
        ctx.send(msg).await?;
    } else {
        ctx.say("Could not identify emoji").await?;
    }
    Ok(())
}

/// Create a poll
#[poise::command(slash_command, guild_only)]
pub async fn poll(
    ctx: Ctx<'_>,
    #[description = "Poll subject"] question: String,
    #[min = 1]
    #[max = 336]
    #[description = "Poll duration in hours (Max 336)"]
    duration: u64,
    #[description = "Option 1"] option01: String,
    #[description = "Option 2"] option02: String,
    #[description = "Option 3"] option03: Option<String>,
    #[description = "Option 4"] option04: Option<String>,
    #[description = "Option 5"] option05: Option<String>,
    #[description = "Option 6"] option06: Option<String>,
    #[description = "Option 7"] option07: Option<String>,
    #[description = "Option 8"] option08: Option<String>,
    #[description = "Option 9"] option09: Option<String>,
    #[description = "Option 10"] option10: Option<String>,
) -> Result<(), Err> {
    let mut answers = vec![
        CreatePollAnswer::new().text(option01),
        CreatePollAnswer::new().text(option02),
    ];

    let optionals = vec![
        option03, option04, option05, option06, option07, option08, option09, option10,
    ];
    for optional in optionals {
        if let Some(opt) = optional {
            answers.push(CreatePollAnswer::new().text(opt));
        }
    }

    let poll = CreatePoll::new()
        .question(question)
        .answers(answers)
        .duration(std::time::Duration::from_secs(duration * 3600));

    let message = CreateMessage::new().poll(poll);
    ctx.channel_id().send_message(&ctx.http(), message).await?;
    ctx.send(
        poise::CreateReply::default()
            .content("Poll created")
            .ephemeral(true),
    )
    .await?;

    Ok(())
}

/// Roll a die
#[poise::command(slash_command)]
pub async fn roll(
    ctx: Ctx<'_>,
    #[description = "Number of sides on the die (Default 6)"]
    #[min = 4]
    #[max = 20]
    die: Option<u8>,
) -> Result<(), Err> {
    let roll_max = die.unwrap_or(6);

    let roll = {
        let mut rng = rand::rng();
        rng.random_range(1..roll_max + 1)
    };
    let msg = format!("D{} roll result is **{}!**", roll_max, roll);

    ctx.say(msg).await?;
    Ok(())
}

/// Roll a D&D style die
#[poise::command(slash_command)]
pub async fn roll_dnd(
    ctx: Ctx<'_>,
    #[description = "Dice to roll"] die: DndDie,
    #[description = "Number of rolls (Default 1)"]
    #[min = 1]
    #[max = 255]
    rolls: Option<u8>,
    #[description = "Roll modifier (Default 0)"]
    // #[min = -128]
    #[max = 127]
    modifier: Option<i8>,
) -> Result<(), Err> {
    // TODO prolly do int conversion
    let die_int = match &die {
        DndDie::D4 => 4,
        DndDie::D6 => 6,
        DndDie::D8 => 8,
        DndDie::D10 => 10,
        DndDie::D12 => 12,
        DndDie::D20 => 20,
    };
    let mut rolls = rolls.unwrap_or(1);
    let modifier = modifier.unwrap_or(0);

    let prefix = if rolls > 1 {
        rolls.to_string()
    } else {
        String::from("")
    };

    let suffix = if modifier == 0 {
        String::from("")
    } else if modifier.is_negative() {
        modifier.to_string()
    } else {
        format!("+{}", modifier)
    };

    let roll = {
        let mut result = 0;
        let mut rng = rand::rng();

        while rolls > 0 {
            result += rng.random_range(1..die_int + 1);
            rolls -= 1;
        }

        result
    };
    let msg = format!(
        "{}{:?}{} roll result is **{}!**",
        prefix,
        die,
        suffix,
        roll + modifier
    );

    ctx.say(msg).await?;
    Ok(())
}

#[derive(Debug, poise::ChoiceParameter)]
enum DndDie {
    #[name = "D4"]
    D4 = 4,
    #[name = "D6"]
    D6 = 6,
    #[name = "D8"]
    D8 = 8,
    #[name = "D10"]
    D10 = 10,
    #[name = "D12"]
    D12 = 12,
    #[name = "D20"]
    D20 = 20,
}
