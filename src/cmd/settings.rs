use crate::{
    dal::guild_settings::{
        create_guild_settings, delete_guild_settings, get_guild_settings, update_guild_settings,
    },
    util::embed::create_default_embed,
    Ctx, Err,
};
use poise::serenity_prelude as serenity;

#[poise::command(
    slash_command,
    subcommands(
        "get",
        "welcome_actions",
        "welcome_role",
        "welcome_channel",
        "optin",
        "optout"
    ),
    required_permissions = "MANAGE_GUILD",
    subcommand_required
)]
pub async fn settings(_: Ctx<'_>) -> Result<(), Err> {
    Ok(())
}

/// Get the current guild settings
#[poise::command(slash_command)]
pub async fn get(ctx: Ctx<'_>) -> Result<(), Err> {
    let guild_id = i64::from(ctx.guild_id().unwrap());
    if let Some(settings) = get_guild_settings(guild_id).await? {
        let welcome_actions = if settings.enable_welcome_actions {
            "Enabled"
        } else {
            "Disabled"
        };
        let channel = match &settings.welcome_channel_id {
            Some(id) => format!("<#{id}>"),
            None => String::from("None"),
        };
        let role = match &settings.welcome_role_id {
            Some(id) => format!("<@&{id}>"),
            None => String::from("None"),
        };

        let welcome_message = match &settings.welcome_message {
            Some(msg) => String::from(msg),
            None => String::from("None"),
        };

        let fields = vec![
            ("welcome_actions", welcome_actions, true),
            ("welcome_channel", channel.as_str(), true),
            ("welcome_role", role.as_str(), true),
            (
                "welcome_message (Supported soon:tm:)",
                welcome_message.as_str(),
                false,
            ),
        ];

        let embed = create_default_embed("Guild Settings".into(), &ctx).fields(fields);
        let msg = poise::CreateReply::default().embed(embed);
        ctx.send(msg).await?;
    } else {
        ctx.say("You are not opted in to guild settings, you can opt in with `/settings optin`")
            .await?;
    }

    Ok(())
}

/// Enable or disable welcome actions
#[poise::command(slash_command)]
pub async fn welcome_actions(
    ctx: Ctx<'_>,
    #[description = "Whether to enable/disable role"] enabled: bool,
) -> Result<(), Err> {
    let guild_id = i64::from(ctx.guild_id().unwrap());
    if let Some(mut settings) = get_guild_settings(guild_id).await? {
        settings.enable_welcome_actions = enabled;
        if let Ok(_) = update_guild_settings(&settings).await {
            let value = if enabled { "enabled" } else { "disabled" };

            ctx.say(format!("Welcome actions {}", value)).await?;
        } else {
            ctx.say("An error occurred updating the setting, please try again later")
                .await?;
        }
    } else {
        ctx.say("You are not opted in to guild settings, you can opt in with `/settings optin`")
            .await?;
    }

    Ok(())
}

/// Set welcome actions autorole
#[poise::command(slash_command)]
pub async fn welcome_role(
    ctx: Ctx<'_>,
    #[description = "Role to set as the \"default\" role when a user joins the server"]
    role: serenity::Role,
) -> Result<(), Err> {
    let guild_id = i64::from(ctx.guild_id().unwrap());
    if let Some(mut settings) = get_guild_settings(guild_id).await? {
        settings.welcome_role_id = Some(i64::from(role.id));
        if let Ok(_) = update_guild_settings(&settings).await {
            let mut msg = format!("Welcome role set to <@&{}>", role.id);
            if !settings.enable_welcome_actions {
                msg += " - Please note, you do not have welcome actions enabled, so autorole will not work until it is enabled. You can enable welcome actions with `/settings welcome_actions`";
            }
            ctx.say(msg).await?;
        } else {
            ctx.say("An error occurred updating the setting, please try again later")
                .await?;
        }
    } else {
        ctx.say("You are not opted in to guild settings, you can opt in with `/settings optin`")
            .await?;
    }

    Ok(())
}

/// Set welcome actions welcome channel
#[poise::command(slash_command)]
pub async fn welcome_channel(
    ctx: Ctx<'_>,
    #[description = "Channel to set as the place to send a welcome message when a user joins a guild"]
    channel: serenity::Channel,
) -> Result<(), Err> {
    let guild_id = i64::from(ctx.guild_id().expect("guild_id not present"));
    if let Some(mut settings) = get_guild_settings(guild_id).await.unwrap() {
        settings.welcome_channel_id = Some(i64::from(channel.id()));
        if let Ok(_) = update_guild_settings(&settings).await {
            let mut msg = format!("Welcome channel set to <#{}>", channel.id());
            if !settings.enable_welcome_actions {
                msg += " - Please note, you do not have welcome actions enabled, so user welcoming will not function until it is enabled. You can enable welcome actions with `/settings welcome_actions`";
            }
            ctx.say(msg).await?;
        } else {
            ctx.say("An error occurred updating the setting, please try again later")
                .await?;
        }
    } else {
        ctx.say("You are not opted in to guild settings, you can opt in with `/settings optin`")
            .await?;
    }

    Ok(())
}

/// Opt in to bot settings
#[poise::command(slash_command)]
pub async fn optin(ctx: Ctx<'_>) -> Result<(), Err> {
    let guild_id = i64::from(ctx.guild_id().expect("guild_id not present"));
    if !is_opted_in(guild_id).await {
        if let Ok(_) = create_guild_settings(guild_id).await {
            ctx.say("You have been opted in to guild settings, you can opt out any time with `/settings optout`").await?;
        } else {
            ctx.say("An error occured opting you in to guild settings, please try again later")
                .await?;
        }
    } else {
        ctx.say("You are already opted in").await?;
    }

    Ok(())
}

/// Opt out of bot settings
#[poise::command(slash_command)]
pub async fn optout(ctx: Ctx<'_>) -> Result<(), Err> {
    let guild_id = i64::from(ctx.guild_id().unwrap());
    if is_opted_in(guild_id).await {
        if let Ok(_) = delete_guild_settings(guild_id).await {
            ctx.say("You have been opted out of guild settings, you can opt back in any time with `/settings optin`").await?;
        } else {
            ctx.say("An error occured opting you out of guild settings, please try again later")
                .await?;
        }
    } else {
        ctx.say("You are already opted out").await?;
    }

    Ok(())
}

async fn is_opted_in(guild_id: i64) -> bool {
    get_guild_settings(guild_id).await.unwrap().is_some()
}
