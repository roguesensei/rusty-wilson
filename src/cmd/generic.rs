use futures::future::try_join;
use poise::reply::CreateReply;
use std::time::SystemTime;

use crate::{
    util::{consts::BOT_VERSION, embed::create_default_embed},
    Ctx, Err,
};

/// A rather polite greeting
#[poise::command(slash_command)]
pub async fn hello(ctx: Ctx<'_>) -> Result<(), Err> {
    let author = &ctx.author().name;
    let msg = format!("Hello there **{author}**");

    ctx.say(msg).await?;
    Ok(())
}

/// Pong!
#[poise::command(slash_command)]
pub async fn ping(ctx: Ctx<'_>) -> Result<(), Err> {
    let before = SystemTime::now();
    let msg = ctx.say("Pong!").await.expect("Could not sent the response");

    let after = SystemTime::now();
    let diff = after
        .duration_since(before)
        .expect("Time is backwards, time to panic!");

    let edit = CreateReply {
        content: format!("Pong! `Time taken: {:?}`", diff).into(),
        ..Default::default()
    };
    msg.edit(ctx, edit).await?;

    Ok(())
}

/// Generate invite link
#[poise::command(slash_command)]
pub async fn invite(ctx: Ctx<'_>) -> Result<(), Err> {
    let invite_url = format!(
        "https://discord.com/oauth2/authorize?client_id={}",
        ctx.http().get_current_user().await.unwrap().id,
    );
    ctx.say(invite_url).await?;

    Ok(())
}

/// Get bot release info
#[poise::command(slash_command)]
pub async fn release(ctx: Ctx<'_>) -> Result<(), Err> {
    if let Ok((info, bot)) = try_join(
        ctx.http().get_current_application_info(),
        ctx.http().get_current_user(),
    )
    .await
    {
        let fields = vec![
            ("Bot Name", bot.name.as_str(), true),
            ("Bot Version", BOT_VERSION, true),
        ];

        let embed = create_default_embed("Release Info".into(), &ctx)
            .fields(fields)
            .thumbnail(bot.avatar_url().unwrap_or_default())
            .image(info.cover_image.unwrap_or_default());

        let msg = poise::CreateReply::default().embed(embed);
        ctx.send(msg).await?;
    } else {
        ctx.say("Could not retreive release info").await?;
    }
    Ok(())
}
