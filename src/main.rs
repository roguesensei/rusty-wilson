mod cmd;
mod dal;
mod util;

use ::serenity::all::ReactionType;
use core::panic;
use dal::guild_settings::get_guild_settings;
use poise::serenity_prelude as serenity;
use reqwest::Client as HttpClient;
use serenity::prelude::*;
use std::{env, sync::Arc};
use util::{
    activity::get_config_activity_defaults,
    config::{load_config, BotConfig},
    consts::{BOT_INTENTS, BOT_TOK_VAR_NAME, DEBUG_GUILD_VAR_NAME},
};

struct Data {
    config: BotConfig,
    http_client: HttpClient,
    songbird: Arc<songbird::Songbird>,
}
type Err = Box<dyn std::error::Error + Send + Sync>;
type Ctx<'a> = poise::Context<'a, Data, Err>;

#[tokio::main]
async fn main() {
    let tok = env::var(BOT_TOK_VAR_NAME).unwrap_or_else(|_| panic!("${BOT_TOK_VAR_NAME} not set"));
    let config = load_config();
    let commands = vec![
        cmd::admin::bot(),
        cmd::fun::roll(),
        cmd::fun::roll_dnd(),
        cmd::fun::server(),
        cmd::fun::emoji(),
        cmd::fun::poll(),
        cmd::fun::user(),
        cmd::generic::hello(),
        cmd::generic::ping(),
        cmd::generic::invite(),
        cmd::generic::release(),
        cmd::moderator::ban(),
        cmd::moderator::kick(),
        cmd::moderator::nick(),
        cmd::moderator::prune(),
        cmd::moderator::role(),
        cmd::settings::settings(),
        cmd::voice::music(),
        cmd::voice::voice(),
    ];

    println!("Migrating DB");
    dal::migrate::run().await;
    println!("DB Migration OK");

    let songbird_manager = songbird::Songbird::serenity();
    let manager_clone = Arc::clone(&songbird_manager);
    let framework = poise::Framework::builder()
        .options(poise::FrameworkOptions {
            commands,
            event_handler: |ctx, event, framework, data| {
                Box::pin(event_handler(ctx, event, framework, data))
            },
            ..Default::default()
        })
        .setup(|ctx, _ready, fw| {
            Box::pin(async move {
                if let Ok(should_reset) = env::var("BOT_STARTUP_CMD_RESET") {
                    if should_reset.trim() == "1" {
                        if let Ok(cmds) = ctx.http().get_global_commands().await {
                        println!("Deleting global commands, this operation may take a while...");
                            for cmd in cmds {
                                if let Err(why) = ctx.http().delete_global_command(cmd.id).await {
                                    eprintln!("{why}");
                                }
                            }
                        } else {
                            eprintln!("Could not fetch the guild commands while trying to delete them");
                        }
                    }
                }
                if let Ok(x) = env::var(DEBUG_GUILD_VAR_NAME) {
                    if let Ok(y) = x.parse::<u64>() {
                        println!("${DEBUG_GUILD_VAR_NAME} identified");
                        let debug_guild = serenity::GuildId::new(y);
                        poise::builtins::register_in_guild(
                            ctx,
                            &fw.options().commands,
                            debug_guild,
                        )
                        .await
                        .expect("Could not sync debug guild commands");
                    } else {
                        panic!(
                            "${DEBUG_GUILD_VAR_NAME} supplied but the value is invalid (must be a snowflake integer)",
                        );
                    }
                } else {
                    println!(
                        "${DEBUG_GUILD_VAR_NAME} not supplied, syncing commands globally"
                    );

                    poise::builtins::register_globally(ctx, &fw.options().commands)
                        .await
                        .expect("Could not register the commands");
                }

                Ok(Data {
                    config,
                    http_client: HttpClient::new(),
                    songbird: manager_clone
                })
            })
        })
        .build();

    let mut client = Client::builder(tok, BOT_INTENTS)
        .voice_manager_arc(songbird_manager)
        .framework(framework)
        .await
        .expect("Could not build the client");

    if let Err(why) = client.start().await {
        panic!("Client error: {why:?}");
    }
}

async fn event_handler(
    ctx: &serenity::Context,
    event: &serenity::FullEvent,
    _framework: poise::FrameworkContext<'_, Data, Err>,
    data: &Data,
) -> Result<(), Err> {
    match event {
        serenity::FullEvent::Ready { data_about_bot, .. } => {
            let (activity, status) = get_config_activity_defaults(data);
            ctx.set_presence(activity, status);

            println!("{} appears...", data_about_bot.user.name);
        }
        serenity::FullEvent::GuildMemberAddition { new_member } => {
            let guild_id = i64::from(new_member.guild_id);
            if let Some(settings) = get_guild_settings(guild_id)
                .await
                .expect("Could not retrieve guild settings")
            {
                if settings.enable_welcome_actions {
                    if let Some(id) = settings.welcome_role_id {
                        let role = id as u64;
                        if let Err(why) = new_member.add_role(&ctx.http(), role).await {
                            eprintln!("{why:?}");
                        }
                    }

                    if let Some(id) = settings.welcome_channel_id {
                        let channel = serenity::ChannelId::new(id as u64);
                        let msg = serenity::CreateMessage::new().content(format!(
                            "Welcome to the server <@{}> - enjoy your stay!",
                            new_member.user.id
                        ));
                        channel.send_message(&ctx.http(), msg).await?;
                    }
                }
            }
        }
        serenity::FullEvent::ReactionAdd { add_reaction } => {
            let emoji = &add_reaction.emoji;
            let user = &add_reaction.user(ctx).await?;
            if !user.bot {
                match emoji {
                    #[allow(unused_variables)]
                    ReactionType::Custom { animated, id, name } => {
                        // TODO
                        println!("{name:?}");
                    }
                    ReactionType::Unicode(u) => {
                        let role_id = dal::autorole::get_autorole_by_unicode_emoji(
                            add_reaction.message_id.get(),
                            u,
                        )
                        .await?;

                        if let Some(rid) = role_id {
                            let rid = serenity::RoleId::new(rid.role_id);
                            ctx.http()
                                .add_member_role(add_reaction.guild_id.unwrap(), user.id, rid, None)
                                .await?;
                        }
                    }
                    _ => {}
                }
            }
        }
        _ => {}
    }
    Ok(())
}
