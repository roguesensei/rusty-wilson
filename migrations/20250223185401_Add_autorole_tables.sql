-- Add migration script here
CREATE TABLE IF NOT EXISTS guild_settings_autorole (
    guild_settings_autorole_id INTEGER PRIMARY KEY NOT NULL,
    guild_settings_id INTEGER NOT NULL,
    message_id BIGINT NOT NULL,
    name VARCHAR(64) NOT NULL,
    FOREIGN KEY(guild_settings_id) REFERENCES guild_settings(guild_settings_id)
);
CREATE INDEX IF NOT EXISTS idx_guild_settings_autorole_guild_settings_id ON guild_settings_autorole (guild_settings_id);
CREATE INDEX IF NOT EXISTS idx_guild_settings_autorole_message_id ON guild_settings_autorole (message_id);

CREATE TABLE IF NOT EXISTS guild_settings_autorole_detail (
    guild_settings_autorole_detail_id INTEGER PRIMARY KEY NOT NULL,
    guild_settings_autorole_id INTEGER NOT NULL,
    reaction_type TINYINT NOT NULL,
    custom_emoji_id BIGINT NULL,
    unicode_emoji_str TEXT,
    role_id BIGINT NOT NULL,
    FOREIGN KEY(guild_settings_autorole_id) REFERENCES guild_settings_autorole(guild_settings_autorole_id)
);
CREATE INDEX IF NOT EXISTS idx_guild_settings_autorole_detail_guild_settings_autorole_id ON guild_settings_autorole_detail (guild_settings_autorole_id);
