-- Add migration script here
CREATE TABLE IF NOT EXISTS guild_settings (
    guild_settings_id INTEGER PRIMARY KEY NOT NULL,
    guild_id BIGINT NOT NULL,
    enable_welcome_actions BOOLEAN NOT NULL DEFAULT(false),
    welcome_message VARCHAR(1024) NULL,
    welcome_channel_id BIGINT NULL,
    welcome_role_id BIGINT NULL
);
CREATE INDEX IF NOT EXISTS idx_guild_settings_guild_id ON guild_settings (guild_id);
